import javax.swing.*;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;


public class DrawingLines extends JFrame {
	//setting the coordinates to 0
	private int xbegin = 0;
	private int ybegin = 0;
	private int xend = 0;
	private int yend = 0;
	
	//building the panel and adding mouse 
	public DrawingLines() {
		setTitle("line");
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		addMouseListener(mouseHandler);
		addMouseMotionListener(mouseMotionHandler);
	}
	
	public static void main(String[] args) {
		
	DrawingLines line = new DrawingLines();
		
	}
	
	//when the mouse press and release
	public MouseListener mouseHandler = new MouseAdapter() {
	
		@Override
		public void mousePressed(MouseEvent e) {
			xbegin = xend = e.getX();
			ybegin = yend = e.getY();
			repaint();
			
	      
		}
		@Override
		public void mouseReleased(MouseEvent e) {
			xend = e.getX();
			yend = e.getY();
			repaint();
			
		}
			
	};
	 
	//when the mouse is moving
	 public MouseMotionListener mouseMotionHandler = new MouseMotionAdapter() {    
		
		     
		@Override
		public void mouseDragged(MouseEvent e) {
			xend = e.getX();
			yend = e.getY();
			
			repaint();
		}
	 };
	 
	 //drawing the line
	 public void paint(Graphics g) {
		 super.paint(g);
		 g.drawLine(xbegin, ybegin, xend, yend);
		 
		  }
}