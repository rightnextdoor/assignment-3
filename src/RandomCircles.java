import acm.program.GraphicsProgram;
import acm.util.RandomGenerator;
import java.awt.Color;

import acm.graphics.GOval;
import acm.program.*;

public class RandomCircles extends GraphicsProgram {
	
	private RandomGenerator rgen = RandomGenerator.getInstance();
	
	public void run() {
		
		
		 
		 for (int i = 0; i < 10; i++) {
			 
			 int radius = rgen.nextInt(5,50); // radius of the circle
			 int numberX = rgen.nextInt(0,500); // random number for x coordinate
			 int numberY = rgen.nextInt(0,500); // random number for y coordinate
			 Color color = rgen.nextColor();
			 
			 GOval circle = new GOval(radius,radius);
			 circle.setFilled(true);
			 circle.setColor(color);
			 add(circle,numberX,numberY);
			 
		 }
		
		
	}
	
	

}
